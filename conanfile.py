from conans import ConanFile, CMake

class Sha1Conan(ConanFile):
    version = "1.0.0"
    name = "sha-1"
    description = "A C++17 SHA-1 implementation."
    license = "Boost Software License - Version 1.0. http://www.boost.org/LICENSE_1_0.txt"
    url = "https://gitlab.com/b110011/sha-1.git"
    exports_sources = "include/b110011/sha1*", "CMakeLists.txt", "cmake/*", "LICENSE.txt"
    settings = "compiler", "build_type", "arch"
    build_policy = "missing"
    author = "Serhii Zinchenko"

    def build(self):
        """Avoid warning on build step"""
        pass

    def package(self):
        """Run CMake install"""
        cmake = CMake(self)
        cmake.configure()
        cmake.install()

    def package_info(self):
        self.info.header_only()
