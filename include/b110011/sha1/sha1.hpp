/*
Copyright 2021 by Serhii Zinchenko

https://gitlab.com/b110011/sha-1

Distributed under the Boost Software License, Version 1.0.
(See accompanying file LICENSE.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
*/

#ifndef __B110011_SHA1_HPP__
#define __B110011_SHA1_HPP__

#include <cstdint>
#include <istream>
#include <string_view>
#include <string>
#include <type_traits>

namespace b110011 {

class Sha1 final
{

public:

    enum : std::size_t
    {
        k_Base64AlphabetSize  = 64,
        k_Base64Size          = 28 + 1,

        k_HexAlphabetSize     = 16,
        k_HexSize             = 40 + 1
    };

    static constexpr std::string_view ms_base64Alphabet =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz"
        "0123456789"
        "+/"
    ;
    static constexpr std::string_view ms_hexAlphabet = "0123456789abcdef";

public:

    constexpr Sha1 () noexcept;


    template<
            typename _T
        ,	typename = std::enable_if_t< std::is_integral_v< _T > >
    >
    constexpr Sha1 & add ( _T _data ) noexcept;

    constexpr Sha1 & add ( void const * const _data, std::size_t _size );

    constexpr Sha1 & add ( std::string_view _string ) noexcept;

    Sha1 & add ( std::istream & _stream );


    constexpr Sha1 const & finalize () noexcept;


    Sha1 const & toBase64 (
        std::string & _dest,
        std::string_view _alphabet = ms_base64Alphabet
    ) const;

    [[nodiscard]]
    std::string toBase64 (
        std::string_view _alphabet = ms_base64Alphabet
    ) const;


    Sha1 const & toHex (
        std::string & _dest,
        std::string_view _alphabet = ms_hexAlphabet
    ) const;

    [[nodiscard]]
    std::string toHex (
        std::string_view _alphabet = ms_hexAlphabet
    ) const;


    constexpr Sha1 & reset () noexcept;

private:

    constexpr void addByteDontCountBits ( std::uint8_t x ) noexcept;

    constexpr void addUInt8 ( std::uint8_t _data ) noexcept;

    template< typename _T >
    constexpr void addIntegral ( _T _data ) noexcept;

    constexpr void processBlock ( std::uint8_t const * _p ) noexcept;

private:

    std::uint32_t m_state[ 5 ];
    std::uint32_t m_i;
    std::uint64_t m_bitsCount;

    /* Note:
        16 - number of 32bit integers per SHA1 block,
        so number of bytes is 64 ( 16 * sizeof( std::uint32_t ) )
    */
    enum : std::size_t
    {
        k_Sha1BlockBytesCount = 64
    };

    std::uint8_t m_buffer[ k_Sha1BlockBytesCount ];

};

template< typename _T >
[[nodiscard]]
std::string
toBase64 ( _T && _object )
{
    return Sha1{}.add( std::forward< _T >( _object ) ).finalize().toBase64();
}

[[nodiscard]]
inline std::string
toBase64 ( void const * const _data, std::size_t _size )
{
    return Sha1{}.add( _data, _size ).finalize().toBase64();
}

template< typename _T >
[[nodiscard]]
std::string
toSha1Hex ( _T && _object )
{
    return Sha1{}.add( std::forward< _T >( _object ) ).finalize().toHex();
}

[[nodiscard]]
inline std::string
toSha1Hex ( void const * const _data, std::size_t _size )
{
    return Sha1{}.add( _data, _size ).finalize().toHex();
}

} // namespace b110011

#include "b110011/sha1/sha1.ipp"

#endif // __B110011_SHA1_HPP__
