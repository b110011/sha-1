/*
Copyright 2021 by Serhii Zinchenko

https://gitlab.com/b110011/sha-1

Distributed under the Boost Software License, Version 1.0.
(See accompanying file LICENSE.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
*/

#ifndef __B110011_SHA1_IPP__
#define __B110011_SHA1_IPP__

#ifndef __B110011_SHA1_HPP__
    #include "b110011/sha1/sha1.hpp"
    #define B110011_SHA1_INLINE
#else
    #define B110011_SHA1_INLINE inline
#endif

namespace b110011 {
namespace {

inline constexpr std::uint32_t
rol32 ( std::uint32_t _x, std::uint32_t _n ) noexcept
{
    return ( _x << _n ) | ( _x >> ( 32 - _n ) );
}

inline constexpr std::uint32_t
makeWord ( std::uint8_t const * _p ) noexcept
{
    return  ( ( std::uint32_t ) _p[ 0 ] << 3 * 8 )
        |   ( ( std::uint32_t ) _p[ 1 ] << 2 * 8 )
        |   ( ( std::uint32_t ) _p[ 2 ] << 1 * 8 )
        |   ( ( std::uint32_t ) _p[ 3 ] << 0 * 8 )
    ;
}

} // namespace

B110011_SHA1_INLINE constexpr
Sha1::Sha1 () noexcept
    :   m_state{ { 0 } }
    ,   m_i{ 0 }
    ,   m_bitsCount{ 0 }
    ,   m_buffer{ { 0 } }
{
    reset();
}

template< typename _T, typename >
constexpr Sha1 &
Sha1::add ( _T _data ) noexcept
{
    addIntegral( _data );
    return *this;
}

B110011_SHA1_INLINE constexpr Sha1 &
Sha1::add ( void const * const _data, std::size_t _size )
{
    if ( !_data || !_size )
    {
        return *this;
    }

    auto ptr{ reinterpret_cast< std::uint8_t const * >( _data ) };

    // Fill up block if not full.
    for ( ; _size && m_i % sizeof( m_buffer ); _size-- )
    {
        add( *ptr++ );
    }

    // Process full blocks.
    for ( ; _size >= sizeof( m_buffer ); _size -= sizeof( m_buffer ) )
    {
        processBlock( ptr );

        ptr += sizeof( m_buffer );
        m_bitsCount += sizeof( m_buffer ) * 8;
    }

    // Process remaining part of block.
    for ( ; _size; _size-- )
    {
        add( *ptr++ );
    }

    return *this;
}

B110011_SHA1_INLINE constexpr Sha1 &
Sha1::add ( std::string_view _string ) noexcept
{
    return add( _string.data(), _string.size() );
}

B110011_SHA1_INLINE Sha1 &
Sha1::add ( std::istream & _stream )
{
    if ( !_stream )
    {
        return *this;
    }

    char block[ k_Sha1BlockBytesCount ];

    while ( _stream.read( block, k_Sha1BlockBytesCount ) )
    {
        add( block, k_Sha1BlockBytesCount );
    }

    if ( auto const lastTimeReadedCharsCount{ _stream.gcount() } )
    {
        add( block, lastTimeReadedCharsCount );
    }

    return *this;
}

B110011_SHA1_INLINE constexpr Sha1 const &
Sha1::finalize () noexcept
{
    // Hashed text ends with 0x80, some padding 0x00 and the length in bits.

    addByteDontCountBits( 0x80 );

    while ( m_i % 64 != 56 )
    {
        addByteDontCountBits( 0x00 );
    }

    add( m_bitsCount );

    return *this;
}

B110011_SHA1_INLINE Sha1 const &
Sha1::toBase64 ( std::string & _dest, std::string_view _alphabet ) const
{
    if ( _alphabet.size() < k_Base64AlphabetSize )
    {
        throw std::runtime_error{ "Passed Base64 alphabet is less than k_Base64AlphabetSize!" };
    }

    std::uint32_t const triples[] = {
        ( ( m_state[ 0 ] & 0xffffff00 ) >> 1 * 8 ),
        ( ( m_state[ 0 ] & 0x000000ff ) << 2 * 8 ) | ( ( m_state[ 1 ] & 0xffff0000 ) >> 2 * 8 ),
        ( ( m_state[ 1 ] & 0x0000ffff ) << 1 * 8 ) | ( ( m_state[ 2 ] & 0xff000000 ) >> 3 * 8 ),
        ( ( m_state[ 2 ] & 0x00ffffff ) << 0 * 8 ),
        ( ( m_state[ 3 ] & 0xffffff00 ) >> 1 * 8 ),
        ( ( m_state[ 3 ] & 0x000000ff ) << 2 * 8 ) | ( ( m_state[ 4 ] & 0xffff0000 ) >> 2 * 8 ),
        ( ( m_state[ 4 ] & 0x0000ffff ) << 1 * 8 )
    };

    for ( std::size_t i{ 0 }; i < 7; i++ )
    {
        auto const x{ triples[ i ] };

        _dest += _alphabet[ ( x >> 3 * 6 ) % 64 ];
        _dest += _alphabet[ ( x >> 2 * 6 ) % 64 ];
        _dest += _alphabet[ ( x >> 1 * 6 ) % 64 ];
        _dest += _alphabet[ ( x >> 0 * 6 ) % 64 ];
    }

    _dest += '=';

    return *this;
}

B110011_SHA1_INLINE std::string
Sha1::toBase64 ( std::string_view _alphabet ) const
{
    std::string result;
    result.reserve( k_Base64Size );

    toBase64( result, _alphabet );

    return result;
}

B110011_SHA1_INLINE Sha1 const &
Sha1::toHex ( std::string & _dest, std::string_view _alphabet ) const
{
    if ( _alphabet.size() < k_HexAlphabetSize )
    {
        throw std::runtime_error{ "Passed hex alphabet is less than k_HexAlphabetSize!" };
    }

    for ( std::size_t i{ 0 }; i < 5; i++ )
    {
        for ( int j{ 7 }; j >= 0; j-- )
        {
            _dest += _alphabet[ ( m_state[ i ] >> j * 4 ) & 0xf ];
        }
    }

    return *this;
}

B110011_SHA1_INLINE std::string
Sha1::toHex ( std::string_view _alphabet ) const
{
    std::string result;
    result.reserve( k_HexSize );

    toHex( result, _alphabet );

    return result;
}

B110011_SHA1_INLINE constexpr Sha1 &
Sha1::reset () noexcept
{
    m_i = 0;
    m_bitsCount = 0;

    m_state[ 0 ] = 0X67'45'23'01;
    m_state[ 1 ] = 0XEF'CD'AB'89;
    m_state[ 2 ] = 0X98'BA'DC'FE;
    m_state[ 3 ] = 0X10'32'54'76;
    m_state[ 4 ] = 0XC3'D2'E1'F0;

    return *this;
}

B110011_SHA1_INLINE constexpr void
Sha1::addByteDontCountBits ( std::uint8_t x ) noexcept
{
    m_buffer[ m_i++ ] = x;

    if ( m_i >= sizeof( m_buffer ) )
    {
        m_i = 0;
        processBlock( m_buffer );
    }
}

B110011_SHA1_INLINE constexpr void
Sha1::addUInt8 ( std::uint8_t _data ) noexcept
{
    addByteDontCountBits( _data );
    m_bitsCount += 8;
}

template< typename _T >
B110011_SHA1_INLINE constexpr void
Sha1::addIntegral ( _T _data ) noexcept
{
    static_assert( std::is_integral_v< _T >, "Only integral types are allowed!" );

    for ( int i{ sizeof( _T ) - 1 }; i >= 0; i-- )
    {
        addUInt8( static_cast< std::uint8_t >( _data >> i * 8 ) );
    }
}

#define B110011_SHA1_LOAD( i )                                                 \
    w[ i & 15 ] = rol32( w[ ( i + 13 ) & 15 ] ^ w[ ( i + 8 ) & 15 ] ^ w[ ( i + 2 ) & 15 ] ^ w[ i & 15 ], 1 );

#define B110011_SHA1_ROUND_0( v, u, x, y, z, i )                               \
    z += ( ( u & ( x ^ y ) ) ^ y ) + w[ i & 15 ] + c0 + rol32( v, 5 );         \
    u = rol32( u, 30 );                                                        \

#define B110011_SHA1_ROUND_1( v, u, x, y, z, i )                               \
    B110011_SHA1_LOAD( i )                                                     \
    z += ( ( u & ( x ^ y ) ) ^ y ) + w[ i & 15 ] + c0 + rol32( v, 5 );         \
    u = rol32( u, 30 );                                                        \

#define B110011_SHA1_ROUND_2( v, u, x, y, z, i )                               \
    B110011_SHA1_LOAD( i )                                                     \
    z += ( u ^ x ^ y ) + w[ i & 15 ] + c1 + rol32( v, 5 );                     \
    u = rol32( u, 30 );                                                        \

#define B110011_SHA1_ROUND_3( v, u, x, y, z, i )                               \
    B110011_SHA1_LOAD( i )                                                     \
    z += ( ( ( u | x ) & y ) | ( u & x ) ) + w[ i & 15 ] + c2 + rol32( v, 5 ); \
    u = rol32(u, 30);                                                          \

#define B110011_SHA1_ROUND_4( v, u, x, y, z, i )                               \
    B110011_SHA1_LOAD( i )                                                     \
    z += ( u ^ x ^ y ) + w[ i & 15 ] + c3 + rol32( v, 5 );                     \
    u = rol32( u, 30 );                                                        \

B110011_SHA1_INLINE constexpr void
Sha1::processBlock ( std::uint8_t const * _p ) noexcept
{
    constexpr std::uint32_t c0{ 0x5A'82'79'99 };
    constexpr std::uint32_t c1{ 0x6E'D9'EB'A1 };
    constexpr std::uint32_t c2{ 0x8F'1B'BC'DC };
    constexpr std::uint32_t c3{ 0xCA'62'C1'D6 };

    std::uint32_t a{ m_state[ 0 ] };
    std::uint32_t b{ m_state[ 1 ] };
    std::uint32_t c{ m_state[ 2 ] };
    std::uint32_t d{ m_state[ 3 ] };
    std::uint32_t e{ m_state[ 4 ] };

    std::uint32_t w[ 16 ] = { 0 };

    for ( std::size_t i{ 0 }; i < 16; i++ )
    {
        w[ i ] = makeWord( _p + i * 4 );
    }

    B110011_SHA1_ROUND_0( a, b, c, d, e,  0 );
    B110011_SHA1_ROUND_0( e, a, b, c, d,  1 );
    B110011_SHA1_ROUND_0( d, e, a, b, c,  2 );
    B110011_SHA1_ROUND_0( c, d, e, a, b,  3 );
    B110011_SHA1_ROUND_0( b, c, d, e, a,  4 );
    B110011_SHA1_ROUND_0( a, b, c, d, e,  5 );
    B110011_SHA1_ROUND_0( e, a, b, c, d,  6 );
    B110011_SHA1_ROUND_0( d, e, a, b, c,  7 );
    B110011_SHA1_ROUND_0( c, d, e, a, b,  8 );
    B110011_SHA1_ROUND_0( b, c, d, e, a,  9 );
    B110011_SHA1_ROUND_0( a, b, c, d, e, 10 );
    B110011_SHA1_ROUND_0( e, a, b, c, d, 11 );
    B110011_SHA1_ROUND_0( d, e, a, b, c, 12 );
    B110011_SHA1_ROUND_0( c, d, e, a, b, 13 );
    B110011_SHA1_ROUND_0( b, c, d, e, a, 14 );
    B110011_SHA1_ROUND_0( a, b, c, d, e, 15 );
    B110011_SHA1_ROUND_1( e, a, b, c, d, 16 );
    B110011_SHA1_ROUND_1( d, e, a, b, c, 17 );
    B110011_SHA1_ROUND_1( c, d, e, a, b, 18 );
    B110011_SHA1_ROUND_1( b, c, d, e, a, 19 );
    B110011_SHA1_ROUND_2( a, b, c, d, e, 20 );
    B110011_SHA1_ROUND_2( e, a, b, c, d, 21 );
    B110011_SHA1_ROUND_2( d, e, a, b, c, 22 );
    B110011_SHA1_ROUND_2( c, d, e, a, b, 23 );
    B110011_SHA1_ROUND_2( b, c, d, e, a, 24 );
    B110011_SHA1_ROUND_2( a, b, c, d, e, 25 );
    B110011_SHA1_ROUND_2( e, a, b, c, d, 26 );
    B110011_SHA1_ROUND_2( d, e, a, b, c, 27 );
    B110011_SHA1_ROUND_2( c, d, e, a, b, 28 );
    B110011_SHA1_ROUND_2( b, c, d, e, a, 29 );
    B110011_SHA1_ROUND_2( a, b, c, d, e, 30 );
    B110011_SHA1_ROUND_2( e, a, b, c, d, 31 );
    B110011_SHA1_ROUND_2( d, e, a, b, c, 32 );
    B110011_SHA1_ROUND_2( c, d, e, a, b, 33 );
    B110011_SHA1_ROUND_2( b, c, d, e, a, 34 );
    B110011_SHA1_ROUND_2( a, b, c, d, e, 35 );
    B110011_SHA1_ROUND_2( e, a, b, c, d, 36 );
    B110011_SHA1_ROUND_2( d, e, a, b, c, 37 );
    B110011_SHA1_ROUND_2( c, d, e, a, b, 38 );
    B110011_SHA1_ROUND_2( b, c, d, e, a, 39 );
    B110011_SHA1_ROUND_3( a, b, c, d, e, 40 );
    B110011_SHA1_ROUND_3( e, a, b, c, d, 41 );
    B110011_SHA1_ROUND_3( d, e, a, b, c, 42 );
    B110011_SHA1_ROUND_3( c, d, e, a, b, 43 );
    B110011_SHA1_ROUND_3( b, c, d, e, a, 44 );
    B110011_SHA1_ROUND_3( a, b, c, d, e, 45 );
    B110011_SHA1_ROUND_3( e, a, b, c, d, 46 );
    B110011_SHA1_ROUND_3( d, e, a, b, c, 47 );
    B110011_SHA1_ROUND_3( c, d, e, a, b, 48 );
    B110011_SHA1_ROUND_3( b, c, d, e, a, 49 );
    B110011_SHA1_ROUND_3( a, b, c, d, e, 50 );
    B110011_SHA1_ROUND_3( e, a, b, c, d, 51 );
    B110011_SHA1_ROUND_3( d, e, a, b, c, 52 );
    B110011_SHA1_ROUND_3( c, d, e, a, b, 53 );
    B110011_SHA1_ROUND_3( b, c, d, e, a, 54 );
    B110011_SHA1_ROUND_3( a, b, c, d, e, 55 );
    B110011_SHA1_ROUND_3( e, a, b, c, d, 56 );
    B110011_SHA1_ROUND_3( d, e, a, b, c, 57 );
    B110011_SHA1_ROUND_3( c, d, e, a, b, 58 );
    B110011_SHA1_ROUND_3( b, c, d, e, a, 59 );
    B110011_SHA1_ROUND_4( a, b, c, d, e, 60 );
    B110011_SHA1_ROUND_4( e, a, b, c, d, 61 );
    B110011_SHA1_ROUND_4( d, e, a, b, c, 62 );
    B110011_SHA1_ROUND_4( c, d, e, a, b, 63 );
    B110011_SHA1_ROUND_4( b, c, d, e, a, 64 );
    B110011_SHA1_ROUND_4( a, b, c, d, e, 65 );
    B110011_SHA1_ROUND_4( e, a, b, c, d, 66 );
    B110011_SHA1_ROUND_4( d, e, a, b, c, 67 );
    B110011_SHA1_ROUND_4( c, d, e, a, b, 68 );
    B110011_SHA1_ROUND_4( b, c, d, e, a, 69 );
    B110011_SHA1_ROUND_4( a, b, c, d, e, 70 );
    B110011_SHA1_ROUND_4( e, a, b, c, d, 71 );
    B110011_SHA1_ROUND_4( d, e, a, b, c, 72 );
    B110011_SHA1_ROUND_4( c, d, e, a, b, 73 );
    B110011_SHA1_ROUND_4( b, c, d, e, a, 74 );
    B110011_SHA1_ROUND_4( a, b, c, d, e, 75 );
    B110011_SHA1_ROUND_4( e, a, b, c, d, 76 );
    B110011_SHA1_ROUND_4( d, e, a, b, c, 77 );
    B110011_SHA1_ROUND_4( c, d, e, a, b, 78 );
    B110011_SHA1_ROUND_4( b, c, d, e, a, 79 );

    m_state[ 0 ] += a;
    m_state[ 1 ] += b;
    m_state[ 2 ] += c;
    m_state[ 3 ] += d;
    m_state[ 4 ] += e;
}

#undef B110011_SHA1_ROUND_4
#undef B110011_SHA1_ROUND_3
#undef B110011_SHA1_ROUND_2
#undef B110011_SHA1_ROUND_1
#undef B110011_SHA1_ROUND_0
#undef B110011_SHA1_LOAD

} // namespace b110011

#undef B110011_SHA1_INLINE

#endif // __B110011_SHA1_IPP__
