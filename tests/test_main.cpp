/*
Copyright 2021 by Serhii Zinchenko

https://gitlab.com/b110011/sha-1

Distributed under the Boost Software License, Version 1.0.
(See accompanying file LICENSE.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
*/

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
