/*
Copyright 2021 by Serhii Zinchenko

https://gitlab.com/b110011/sha-1

Distributed under the Boost Software License, Version 1.0.
(See accompanying file LICENSE.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
*/

#include "b110011/sha1/sha1.hpp"

#include <catch2/catch.hpp>

#include <sstream>

/*
Test plan:

Done    1. Build-in types
Done        1.1. Boolean type
Done        1.2. Integer types
Done            1.2.1. short
Done            1.2.2. unsigned short
Done            1.2.3. int
Done            1.2.4. unsigned int
Done            1.2.5. long
Done            1.2.6. unsigned long
Done            1.2.7. long long
Done            1.2.8. unsigned long long
            1.3. Floating point types
                1.3.1. float
                1.3.2. double
Done        1.4. Char type
Done    2. Test vectors
Done        2.1. empty string
Done        2.2. abc
Done        2.3. abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq
Done        2.4. abcdefghbcdefghicdefghijdefghijkefghijklfghijklmghijklmnhijklmnoijklmnopjklmnopqklmnopqrlmnopqrsmnopqrstnopqrstu
Done        2.5. 1'000'000 repetitions of 'a'
Done        2.6. 16'777'216 repetitions of abcdefghbcdefghicdefghijdefghijkefghijklfghijklmghijklmnhijklmno
Done        2.7. The quick brown fox jumps over the lazy dog
Done    3. Dummy tests
Done        3.1. Two instances give same hash for same data
Done        3.2. Two instances give different hash for different data
Done        3.3. One instance, several add
Done        3.4. One instance, several finalize
Done        3.5. One instance, several reset
Done    4. Input stream
Done        4.1. Text format
Done        4.2. Binary format

Note 1:
    sha1 hashes for boolean, integer and char types obtained using this python libraries:

    import hashlib
    from struct import pack, unpack

    All code snippets attached to each test case.

Note 2:
    Test vectors: https://www.di-mgt.com.au/sha_testvectors.html
*/

namespace b110011 {

TEST_CASE( "1.1. Boolean type", "[Utils][Sha1][Build-in types]" )
{
    // hashlib.sha1(bytearray(unpack('1B', pack('>?', False)))).hexdigest()
    CHECK( toSha1Hex( false ) == "5ba93c9db0cff93f52b521d7420e43f6eda2784f" );

    // hashlib.sha1(bytearray(unpack('1B', pack('>?', True)))).hexdigest()
    CHECK( toSha1Hex( true ) == "bf8b4530d8d246dd74ac53a13471bba17941dff7" );
}

TEST_CASE( "1.2.1. short", "[Utils][Sha1][Build-in types][Integral types]" )
{
    // hashlib.sha1(bytearray(unpack('2B', pack('>h', 1234)))).hexdigest()
    CHECK( toSha1Hex( static_cast< short >( 1234 ) ) == "1a205d2c722d9baaa5e9312140d40a308db5bce9" );
}

TEST_CASE( "1.2.2. unsigned short", "[Utils][Sha1][Build-in types][Integral types]" )
{
    // hashlib.sha1(bytearray(unpack('2B', pack('>H', 1234)))).hexdigest()
    CHECK( toSha1Hex( static_cast< unsigned short >( 1234 ) ) == "1a205d2c722d9baaa5e9312140d40a308db5bce9" );
}

TEST_CASE( "1.2.3. int", "[Utils][Sha1][Build-in types][Integral types]" )
{
    // hashlib.sha1(bytearray(unpack('4B', pack('>i', 1234)))).hexdigest()
    CHECK( toSha1Hex( 1234 ) == "ac9928e78cf6ea117451ecd6654fea2adae73e21" );
}

TEST_CASE( "1.2.4. unsigned int", "[Utils][Sha1][Build-in types][Integral types]" )
{
    // hashlib.sha1(bytearray(unpack('4B', pack('>I', 1234)))).hexdigest()
    CHECK ( toSha1Hex( 1234u ) == "ac9928e78cf6ea117451ecd6654fea2adae73e21" );
}

TEST_CASE( "1.2.5. long", "[Utils][Sha1][Build-in types][Integral types]" )
{
    // hashlib.sha1(bytearray(unpack('4B', pack('>l', 1234)))).hexdigest()
    CHECK( toSha1Hex( 1234l ) == "ac9928e78cf6ea117451ecd6654fea2adae73e21" );
}

TEST_CASE( "1.2.6. unsigned long", "[Utils][Sha1][Build-in types][Integral types]" )
{
    // hashlib.sha1(bytearray(unpack('4B', pack('>L', 1234)))).hexdigest()
    CHECK( toSha1Hex( 1234lu ) == "ac9928e78cf6ea117451ecd6654fea2adae73e21" );
}

TEST_CASE( "1.2.7. long long", "[Utils][Sha1][Build-in types][Integral types]" )
{
    // hashlib.sha1(bytearray(unpack('8B', pack('>q', 1234)))).hexdigest()
    CHECK( toSha1Hex( 1234ll ) == "b0d48de85d5d1cc32128e49ab1066d85b75953b8" );
}

TEST_CASE( "1.2.8. unsigned long long", "[Utils][Sha1][Build-in types][Integral types]" )
{
    // hashlib.sha1(bytearray(unpack('8B', pack('>Q', 1234)))).hexdigest()
    CHECK( toSha1Hex ( 1234llu ) == "b0d48de85d5d1cc32128e49ab1066d85b75953b8" );
}
/*
TEST_CASE( "1.3.1. float", "[Utils][Sha1][Build-in types][Floating point types]" )
{
    // hashlib.sha1(bytearray(unpack('4B', pack('>f', 12.34)))).hexdigest()
    CHECK( toSha1Hex( 12.34f ) == "0916e3cb9a41f16a626231790ddc3176a1767c7c" );
}

TEST_CASE( "1.3.2. double", "[Utils][Sha1][Build-in types][Floating point types]" )
{
    // hashlib.sha1(bytearray(unpack('8B', pack('>d', 12.34)))).hexdigest()
    CHECK( toSha1Hex( 12.34 ) == "f52dbd24ee6dfdcc8ba9594a60f5319b5b9e1d6a" );
}
*/
TEST_CASE( "1.4. Char type", "[Utils][Sha1]" )
{
    CHECK( toSha1Hex( 'a' ) == "86f7e437faa5a7fce15d1ddcb9eaeaea377667b8" );
    CHECK( toSha1Hex( 'b' ) == "e9d71f5ee7c92d6dc9e92ffdad17b8bd49418f98" );
    CHECK( toSha1Hex( 'c' ) == "84a516841ba77a5b4648de2cd0dfcb30ea46dbb4" );

    CHECK( toSha1Hex( 'A' ) == "6dcd4ce23d88e2ee9568ba546c007c63d9131c1b" );
    CHECK( toSha1Hex( 'B' ) == "ae4f281df5a5d0ff3cad6371f76d5c29b6d953ec" );
    CHECK( toSha1Hex( 'C' ) == "32096c2e0eff33d844ee6d675407ace18289357d" );
}

TEST_CASE( "2.1. empty string", "[Utils][Sha1][Test vectors]" )
{
    CHECK( toSha1Hex( "" ) == "da39a3ee5e6b4b0d3255bfef95601890afd80709" );
}

TEST_CASE( "2.2. abc", "[Utils][Sha1][Test vectors]" )
{
    CHECK( toSha1Hex( "abc" ) == "a9993e364706816aba3e25717850c26c9cd0d89d" );
}

TEST_CASE(
        "2.3. abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq"
    ,   "[Utils][Sha1][Test vectors]"
)
{
    CHECK(  toSha1Hex( "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq" )
        ==  "84983e441c3bd26ebaae4aa1f95129e5e54670f1"
    );
}

TEST_CASE(
        "2.4. abcdefghbcdefghicdefghijdefghijkefghijklfghijklmghijklmnhijklmnoijklmnopjklmnopqklmnopqrlmnopqrsmnopqrstnopqrstu"
    ,   "[Utils][Sha1][Test vectors]"
)
{
    CHECK(  toSha1Hex( "abcdefghbcdefghicdefghijdefghijkefghijklfghijklmghijklmnhijklmnoijklmnopjklmnopqklmnopqrlmnopqrsmnopqrstnopqrstu" )
        ==  "a49b2446a02c645bf419f995b67091253a04a259"
    );
}

TEST_CASE( "2.5. 1'000'000 repetitions of 'a'", "[Utils][Sha1][Test vectors]" )
{
    Sha1 hasher;
    std::string a100( 100, 'a' );

    for ( std::size_t i{ 0 }; i < 1'000'000; i += 100 )
    {
        hasher.add( a100 );
    }

    hasher.finalize();

    CHECK( hasher.toHex() == "34aa973cd4c4daa4f61eeb2bdbad27316534016f" );
}

TEST_CASE(
        "2.6. 16'777'216 repetitions of abcdefghbcdefghicdefghijdefghijkefghijklfghijklmghijklmnhijklmno"
    ,   "[.][Utils][Sha1][Test vectors]"
)
{
    Sha1 hasher;

    for ( std::size_t i{ 0 }; i < 16'777'216; ++i )
    {
        hasher.add( "abcdefghbcdefghicdefghijdefghijkefghijklfghijklmghijklmnhijklmno" );
    }

    hasher.finalize();

    CHECK( hasher.toHex() == "7789f0c9ef7bfc40d93311143dfbe69e2017f592" );
}

TEST_CASE( "2.7. The quick brown fox jumps over the lazy dog", "[Utils][Sha1][Test vectors]" )
{
    CHECK(  toSha1Hex( "The quick brown fox jumps over the lazy dog" )
        ==  "2fd4e1c67a2d28fced849ee1bb76e7391b93eb12"
    );
}

TEST_CASE( "3.1. Two instances give same hash for same data", "[Utils][Sha1][Dummy tests]" )
{
    // Boolean type

    CHECK( toSha1Hex( true ) == toSha1Hex( true ) );
    CHECK( toSha1Hex( false ) == toSha1Hex( false ) );

    // Integer types

    CHECK( toSha1Hex( -1 ) == toSha1Hex( -1 ) );
    CHECK( toSha1Hex( 0 ) == toSha1Hex( 0 ) );
    CHECK( toSha1Hex( 1 ) == toSha1Hex( 1 ) );

    // Char type

    CHECK( toSha1Hex( 'a' ) == toSha1Hex( 'a' ) );
    CHECK( toSha1Hex( 'Z' ) == toSha1Hex( 'Z' ) );

    // String

    CHECK( toSha1Hex( "" ) == toSha1Hex( "" ) );
    CHECK( toSha1Hex( "abc" ) == toSha1Hex( "abc" ) );
}

TEST_CASE( "3.2. Two instances give different hash for different data", "[Utils][Sha1][Dummy tests]" )
{
    // Boolean type

    CHECK( toSha1Hex( true ) != toSha1Hex( false ) );

    // Integer types

    CHECK( toSha1Hex( 0 ) != toSha1Hex( 1 ) );
    CHECK( toSha1Hex( -1 ) != toSha1Hex( 1 ) );
    CHECK( toSha1Hex( 1234 ) != toSha1Hex( 4321 ) );
    CHECK( toSha1Hex( 1234 ) != toSha1Hex( 3412 ) );

    // Char type

    CHECK( toSha1Hex( 'a' ) != toSha1Hex( 'A' ) );
    CHECK( toSha1Hex( 'z' ) != toSha1Hex( 'M' ) );
    CHECK( toSha1Hex( '\0' ) != toSha1Hex( '\n' ) );

    // String

    CHECK( toSha1Hex( "abc" ) != toSha1Hex( "cba" ) );
}

TEST_CASE( "3.3. One instance, several add", "[Utils][Sha1][Dummy tests]" )
{
    Sha1 hasher;
    hasher
        .add( true )
        .add( 10'958 )
        .add( false )
        .add( "Catch2 is cool testing library!" )
        .add( true )
        .add( '?' )
        .add( false )
        .add( 0xDEAD )
        .add( true )
        .finalize()
    ;

    CHECK( hasher.toHex() == "840d52860c63f87eb5314cabb0497c0f5a618da1" );
}

TEST_CASE ( "3.4. One instance, several finalize", "[Utils][Sha1][Dummy tests]" )
{
    Sha1 hasher;

    std::string hex;
    hex.reserve( Sha1::k_HexSize );

    // 1

    hasher.add( true ).finalize().toHex( hex );
    CHECK( hex == "bf8b4530d8d246dd74ac53a13471bba17941dff7" );

    hex.clear();

    // 2

    hasher.add( 1234 ).finalize().toHex( hex );
    CHECK( hex == "59628c810cc1eea229462d1b7c91d1a505d0df06" );

    hex.clear();

    // 3

    hasher.add( 'a' ).finalize().toHex( hex );
    CHECK( hex == "647ed670d17e3e1af814391793be43900f905fd9" );
}

TEST_CASE ( "3.5. One instance, several reset", "[Utils][Sha1][Dummy tests]" )
{
    Sha1 hasher;

    std::string hex;
    hex.reserve( Sha1::k_HexSize );

    // 1

    hasher.add( true ).finalize().toHex( hex );
    CHECK( hex == "bf8b4530d8d246dd74ac53a13471bba17941dff7" );

    hasher.reset();
    hex.clear();

    // 2

    hasher.add( 1234 ).finalize().toHex( hex );
    CHECK( hex == "ac9928e78cf6ea117451ecd6654fea2adae73e21" );

    hasher.reset();
    hex.clear();

    // 3

    hasher.add( 'a' ).finalize().toHex( hex );
    CHECK( hex == "86f7e437faa5a7fce15d1ddcb9eaeaea377667b8" );
}

TEST_CASE( "4.1. Text format", "[Utils][Sha1][Input stream]" )
{
    std::istringstream iss{ "The quick brown fox jumps over the lazy dog" };
    CHECK( toSha1Hex( iss ) == "2fd4e1c67a2d28fced849ee1bb76e7391b93eb12" );
}

TEST_CASE( "4.2. Binary format", "[Utils][Sha1][Input stream]" )
{
    std::istringstream iss{
            "The quick brown fox jumps over the lazy dog"
        ,   std::ios::in | std::ios::binary
    };

    CHECK( toSha1Hex( iss ) == "2fd4e1c67a2d28fced849ee1bb76e7391b93eb12" );
}

} // namespace b110011

