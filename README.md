# SHA-1 implementation in C++

[SHA-1](https://en.wikipedia.org/wiki/SHA-1) (Secure Hash Algorithm 1) is a cryptographic hash function which takes an input and produces a 160-bit (20-byte) hash value – typically rendered as a hexadecimal number, 40 digits long. It was designed by the United States National Security Agency, and is a U.S. Federal Information Processing Standard.

## Warning

Do not use SHA-1 unless you have to! [SHA-1 is practically broken](https://en.wikipedia.org/wiki/SHA-1#Birthday-Near-Collision_Attack_%E2%80%93_first_practical_chosen-prefix_attack). Use a hash function from the [SHA-2](https://en.wikipedia.org/wiki/SHA-2) or [SHA-3](https://en.wikipedia.org/wiki/SHA-3) family instead.

## Features

* C++17
* Header-only
* Dependency-free

## Example usage

To build the provided example you need to do the below commands in your terminal.

```bash
$ cmake -S. -Bbuild -DCMAKE_INSTALL_PREFIX=install
$ cmake --build build/ --target install
$ cd example/
$ cmake -S. -Bbuild -DCMAKE_INSTALL_PREFIX=../install
```

## Dependencies

This library has no other dependencies than the [C++ standard library](http://en.cppreference.com/w/cpp/header).

## Installation and use

This library is a single-file header-only library. Put `sha1.hpp` and `sha1.ipp` in the [include](include/) folder directly into the project source tree or somewhere reachable from your project.

## Authoring

This library is heavily based on [983/SHA1](https://github.com/983/SHA1) and [vog/sha1](https://github.com/vog/sha1) projects.

## License

This library is distributed under the [Boost Software License](LICENSE).
