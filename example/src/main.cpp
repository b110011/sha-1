/*
Copyright 2021 by Serhii Zinchenko

https://gitlab.com/b110011/sha-1

Distributed under the Boost Software License, Version 1.0.
(See accompanying file LICENSE.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
*/

#include "b110011/sha1/sha1.hpp"

#include <iostream>

int main ( int /* argc */, const char ** /* argv */ )
{
    std::cout << "The SHA-1 of \"abc\" is: " << b110011::toSha1Hex( "abc" ) << std::endl;
    return EXIT_SUCCESS;
}
